(function () {
    'use strict';

    angular.module('app').controller('PostCtrl', Ctrl);

    function Ctrl(http) {

        var vm = this;
        vm.save = save;
        vm.deletePost = deletePost;
        vm.editPost = editPost;
        vm.cleanForm = cleanForm;

        vm.post = {};
        vm.posts = [];

        init();

        function init() {
            http.get('api/posts').then(function (data) {
                vm.posts = data;
            });
        }

        function save(post) {
            if (post.id) {
                var method = http.put;
                var url = 'api/posts?id=' + post.id;
            } else {
                var method = http.post;
                var url = 'api/posts';
            }

            method(url, vm.post).then(function () {
                vm.post = {};
                init();
            }, errorHandler);
        }

        function editPost(postId) {
            http.get('api/posts?id=' + postId).then(function (data) {
                vm.post = data;
            });
        }

        function cleanForm() {
            vm.post = {};
        }

        function deletePost(postId) {
            http.delete('api/posts?id=' + postId).then(function () {
                init();
            }, errorHandler);
        }

        function errorHandler(response) {
            console.log('Error: ' + JSON.stringify(response.data));
        }
    }

})();
