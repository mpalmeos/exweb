(function () {
    'use strict';

    angular.module('app').factory('http', Service);

    function Service($http, $q) {

        var POST = $http.post;
        var GET = $http.get;
        var DELETE = $http.delete;
        var PUT = $http.put;

        return {
            post: post,
            get: get,
            delete: del,
            put: put
        };

        function get(path) {
            return httpCommon(GET, path);
        }

        function del(path) {
            return httpCommon(DELETE, path);
        }

        function post(path, data) {
            return httpCommon(POST, path, data);
        }

        function put(path, data) {
            return httpCommon(PUT, path, data);
        }

        function httpCommon(method, path, data) {
            var url = path;

            if (method === POST || method === PUT) {
                console.debug("data: " + angular.toJson(data));
            }

            var deferred = $q.defer();

            method(url, data).then(
                function(response) {
                    deferred.resolve(response.data);
                },
                function(response, status) {
                    deferred.reject(response);
                }
            );

            return deferred.promise;
        }
    }

})();
