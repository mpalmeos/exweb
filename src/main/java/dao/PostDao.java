package dao;

import exweb.Post;
import util.DataSourceProvider;
import util.PropertyLoader;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostDao {

    public PostDao() {
        String dbUrl = PropertyLoader.getProperty("jdbc.url");
        DataSourceProvider.setDbUrl(dbUrl);
    }

    public List<Post> getPosts() {

        List<Post> posts = new ArrayList<>();

        try (Connection conn = DataSourceProvider.getDataSource().getConnection();
             Statement stmt = conn.createStatement()) {

            String sql = "select id, title, text from post";

            try(ResultSet r = stmt.executeQuery(sql)){
                while(r.next()){
                    posts.add(new Post(
                            r.getLong("id"),
                            r.getString("title"),
                            r.getString("text")
                            ));
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return posts;
    }

    public void save(Post post) {
        String sql = "insert into post (id, title, text) values (next value for seq1, ?, ?)";

        try (Connection conn = DataSourceProvider.getDataSource().getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, post.getTitle());
            ps.setString(2, post.getText());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deletePost(long id) {
    }
}
