package exweb;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.PostDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/posts")
public class PostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {

        PostDao dao = new PostDao();

        new ObjectMapper().writeValue(response.getOutputStream(), dao.getPosts());
    }

    @Override
    protected void doPost(HttpServletRequest req,
                          HttpServletResponse resp)
            throws ServletException, IOException {

        Post post = new ObjectMapper().readValue(req.getInputStream(), Post.class);

        new PostDao().save(post);
    }

    @Override
    protected void doDelete(HttpServletRequest req,
                            HttpServletResponse resp)
            throws ServletException, IOException {

        new PostDao().deletePost(Long.parseLong(req.getParameter("id")));
    }

    private static Long parseLong(String input) {
        try {
            return Long.parseLong(input);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
