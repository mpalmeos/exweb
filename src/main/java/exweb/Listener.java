package exweb;

import dao.SetupDao;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionBindingEvent;

@WebListener()
public class Listener implements ServletContextListener {

   @Override
   public void contextInitialized(ServletContextEvent sce) {
      new SetupDao().initDatabase();
   }

   @Override
   public void contextDestroyed(ServletContextEvent sce) {

   }
}
